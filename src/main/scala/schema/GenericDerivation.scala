package schema

import cats.syntax.functor._
import io.circe.generic.auto._
import io.circe.syntax._
import io.circe.{Decoder, Encoder}


object GenericDerivation {
  implicit val jsonEncoder: Encoder[Schema] = Encoder.instance {
    //register
    case registerRequest@RegisterRequest(_) => registerRequest.asJson
    case registerResponse@RegisterResponse(_, _, _) => registerResponse.asJson
    case acc@Account(_, _) => acc.asJson

    //Errors
    case payloadError@PayloadError(_, _) => payloadError.asJson
    case errorResponse@ErrorResponse(_, _, _) => errorResponse.asJson
    case raiseException@RaiseException(_) => raiseException.asJson

    //Empty
    case emptyPayload@EmptyPayload() => emptyPayload.asJson
    case emptyResponse@EmptyResponse(_, _, _) => emptyResponse.asJson

    //portfolio
    case portfolioResponse@PortfolioResponse(_, _, _) => portfolioResponse.asJson
    case portfolio@Portfolio(_) => portfolio.asJson
    case positions@PortfolioPosition(_, _, _, _, _, _, _, _, _, _, _) => positions.asJson

    case moneyAmount@MoneyAmount(_, _) => moneyAmount.asJson

    //market
    case marketInstrument@MarketInstrument(_, _, _, _, _, _, _, _, _) => marketInstrument.asJson
    case marketInstrumentList@MarketInstrumentList(_, _) => marketInstrumentList.asJson
    case marketInstrumentListResponse@MarketInstrumentListResponse(_, _, _) => marketInstrumentListResponse.asJson

    //Orders
    case marketOrderRequest@MarketOrderRequest(_, _) => marketOrderRequest.asJson
    case limitOrderRequest@LimitOrderRequest(_, _, _) => limitOrderRequest.asJson
    case orderResponse@OrderResponse(_, _, _) => orderResponse.asJson
    case placedOrder@PlacedOrder(_, _, _, _, _, _, _, _) => placedOrder.asJson
    case order@Order(_, _, _, _, _, _, _) => order.asJson

    //sandbox
    case sandboxSetPositionBalanceRequest@SandboxSetPositionBalanceRequest(_, _) => sandboxSetPositionBalanceRequest.asJson

  }

  implicit val jsonDecoder: Decoder[Schema] =
    List[Decoder[Schema]](
      Decoder[RegisterRequest].widen,
      Decoder[RegisterResponse].widen,
      Decoder[Account].widen,

      Decoder[PayloadError].widen,
      Decoder[ErrorResponse].widen,
      Decoder[RaiseException].widen,

      Decoder[PortfolioResponse].widen,
      Decoder[Portfolio].widen,
      Decoder[PortfolioPosition].widen,
      Decoder[MoneyAmount].widen,

      Decoder[MarketInstrument].widen,
      Decoder[MarketInstrumentList].widen,
      Decoder[MarketInstrumentListResponse].widen,

      Decoder[MarketOrderRequest].widen,
      Decoder[LimitOrderRequest].widen,
      Decoder[OrderResponse].widen,
      Decoder[PlacedOrder].widen,
      Decoder[Order].widen,

      Decoder[SandboxSetPositionBalanceRequest].widen

    ).reduceLeft(_ or _)
}