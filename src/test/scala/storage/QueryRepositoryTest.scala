package storage

import org.scalatest.matchers.should.Matchers
import storage.SLTPOrdersQueryRepository._

class QueryRepositoryTest extends DatabaseSuite with Matchers {
  import UserQueryRepository._

  test("findUser") {
    for {
      user <- findUser("1")
    } yield assert(user.contains(Users("1", "token", "tf_id_1")))
  }

  test("addUser") {
    val newUser = Users("2", "token2", "broker2")
    for {
      _ <- addUser(newUser)
      user <- findUser("2")
    } yield assert(user.contains(newUser))
  }

  test("findSLTPOrders") {
    for {
      order <- findSLTPOrders("213")
    } yield assert(order.contains(SampleOrders.head))
  }

  test("addSLTPOrders") {
    //random gen!
    val newOrder = SLTPOrders("broker2", "333", "New")
    for {
      _ <- addSLTPOrders(newOrder)
      order <- findSLTPOrders("333")
    } yield assert(order.contains(newOrder))
  }

  test("findUserOrders") {
    val brokerAccId = "tf_id_1"
    for {
      userOrders <- findUserOrders(brokerAccId)
    } yield assert(userOrders.get(Users("1", "token", "tf_id_1")) match {
      case Some(vector) => vector.contains(SLTPOrders("tf_id_1","213","New"))
      case _ => false
    })
  }

}
