
import akka.actor.ActorSystem
import akka.http.javadsl.model.HttpMethods
import akka.http.scaladsl.Http
import akka.http.scaladsl._
import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.{Authorization, BasicHttpCredentials, OAuth2BearerToken}
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpHeader, HttpMethods, HttpRequest, HttpResponse}
import akka.http.scaladsl.server.{Route, RouteConcatenation}
import akka.http.scaladsl.unmarshalling.{FromResponseUnmarshaller, Unmarshal}
import akka.stream.ActorMaterializer
import com.softwaremill.macwire.{wire, wireSet}
import io.circe._
import io.circe.generic.auto._
import GenericDerivation._
import akka.http.scaladsl.model.StatusCodes.OK
import akka.http.scaladsl.model.Uri._
import io.circe.parser._
import io.circe.syntax._

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success}
import com.typesafe.scalalogging.LazyLogging


class ApiClient(host: Uri, token: String) extends LazyLogging {

  import akka.http.scaladsl.server.Directives._
  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

  implicit val actorSystem: ActorSystem = ActorSystem()
  implicit val ec: ExecutionContext = actorSystem.dispatcher
  implicit val materializer: ActorMaterializer.type = ActorMaterializer

  val auth: Authorization = headers.Authorization(OAuth2BearerToken(token))

  def request(method: HttpMethod, uri: Uri, headers: Seq[HttpHeader], entity: RequestEntity, params: Map[String, String])
             (implicit actorSystem: ActorSystem, ec: ExecutionContext):
  Future[HttpResponse] = {
    logRequest(uri, method)
    Http().singleRequest(HttpRequest(method, uri = uri.withQuery(Query(params)), entity = entity, headers = headers))
  }

//  def response[U: FromResponseUnmarshaller](response: HttpResponse)
//                                           (implicit actorSystem: ActorSystem, ec: ExecutionContext): Future[U] = {
//    logResponse(response)
//    Unmarshal(response).to[U]
//  }

  def callApi[U: FromResponseUnmarshaller](method: HttpMethod,
                                           resource: String,
                                           entity: RequestEntity = HttpEntity.empty(ContentTypes.`application/json`),
                                           headers: Seq[HttpHeader] = Seq.empty,
                                           params: Map[String, String] = Map.empty): Future[U] = {

    request(method, host + resource, auth +: headers, entity, params).flatMap(res =>
      res.status match {
        case OK => {
          logResponse(res)
          Unmarshal(res).to[U]
        }
        case _ => Future.failed(new Throwable(s"${res.status.value}: ${res.status.defaultMessage()}"))
      })
  }


  //logging
  private def logRequest(uri: Uri, method: HttpMethod): Unit =
    logger.debug(s"Http request sent: ${method.value} $uri")

  private def logResponse(response: HttpResponse): Unit =
    logger.debug(s"Received response: code=${response.status}")


}
