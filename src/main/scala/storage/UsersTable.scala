package storage

import slick.dbio.Effect
import slick.jdbc.SQLiteProfile.api._
import slick.lifted.ProvenShape


case class Users (
  chatId: String,
  token: String,
  brokerAccountId: String
)

class UsersTable(tag: Tag) extends Table[Users](tag, "USERS") {

  def chatId: Rep[String] = column("CHAT_ID", O.PrimaryKey)

  def token: Rep[String] = column("TOKEN")
  def brokerAccountId: Rep[String] = column("BROKER_ACCOUNT_ID")

  override def * : ProvenShape[Users] = (chatId, token, brokerAccountId).mapTo[Users]
}

object UserQueryRepository {
  val AllUsers = TableQuery[UsersTable]

  def findUser(chatId: String): DIO[Option[Users], Effect.Read] =
    AllUsers
      .filter(_.chatId === chatId)
      .result
      .headOption

  def addUser(user: Users): DIO[Int, Effect.Write] =
    AllUsers += user
}
