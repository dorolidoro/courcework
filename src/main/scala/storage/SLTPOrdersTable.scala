package storage

import slick.dbio.Effect
import slick.jdbc.SQLiteProfile.api._
import slick.lifted.ProvenShape

import scala.concurrent.ExecutionContext
//import entrypoint.Main._


case class SLTPOrders (
                        brokerAccountId: String,
                        orderId: String,
                        status: String
                )

class SLTPOrdersTable(tag: Tag) extends Table[SLTPOrders](tag, "SLTP_ORDERS") {

  def brokerAccountId: Rep[String] = column("BROKER_ACCOUNT_ID")

  def orderId: Rep[String] = column("ORDER_ID", O.PrimaryKey)
  def status: Rep[String] = column("STATUS")

  override def * : ProvenShape[SLTPOrders] = (brokerAccountId, orderId, status).mapTo[SLTPOrders]
}

object SLTPOrdersQueryRepository {
  val AllSLTPOrders = TableQuery[SLTPOrdersTable]
  val AllUsers = TableQuery[UsersTable]

  def findSLTPOrders(orderId: String): DIO[Option[SLTPOrders], Effect.Read] =
    AllSLTPOrders
      .filter(_.orderId === orderId)
      .result
      .headOption

  def addSLTPOrders(SLTPOrders: SLTPOrders): DIO[Int, Effect.Write] =
    AllSLTPOrders += SLTPOrders

  def findUserOrders(brokerAccountId: String)(implicit ec: ExecutionContext):
  DBIOAction[Map[Users, Seq[SLTPOrders]], NoStream, Effect.Read] ={
    AllSLTPOrders.filter(_.brokerAccountId === brokerAccountId)
      .join(AllUsers)
      .on(_.brokerAccountId === _.brokerAccountId )
      .result
      .map(_.groupMap(_._2)(_._1))


  }
}
