package schema

case class EmptyPayload() extends Schema
case class EmptyResponse(trackingId: String, status: String, payload: EmptyPayload) extends Schema



