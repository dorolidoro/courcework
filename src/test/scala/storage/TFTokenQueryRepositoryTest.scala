package storage

import org.scalatest.matchers.should.Matchers

class TFTokenQueryRepositoryTest extends TFTokenDatabaseSuite with Matchers {
  import TFTokenQueryRepository._

  test("findUser") {
    for {
      user <- findUser(1)
    } yield assert(user.contains(SampleUsers.head))
  }

}
