import cats.syntax.functor._
import io.circe.{Decoder, Encoder}
import io.circe.generic.auto._
import io.circe.syntax._

sealed trait Schema

sealed trait RequestSchema

sealed trait ResponseSchema

case class Payload(brokerAccountType: String, brokerAccountId: String) extends Schema

case class RegisterResponse(trackingId: String, status: String, payload: Payload) extends Schema

case class RegisterRequest(brokerAccountType: String) extends Schema

case class RaiseException(message: String) extends Schema

case class CurrVal (
                           currency: String,
                           value: Int
                         ) extends Schema

case class PayloadPositions (
                     positions: Seq[Positions]
                   ) extends Schema

case class Positions (
                       figi: String,
                       ticker: String,
                       isin: String,
                       instrumentType: String,
                       balance: Int,
                       blocked: Int,
                       expectedYield: CurrVal,
                       lots: Int,
                       averagePositionPrice: CurrVal,
                       averagePositionPriceNoNkd: CurrVal,
                       name: String
                     ) extends Schema

case class PortfolioResponse (
                           trackingId: String,
                           status: String,
                           payload: PayloadPositions
                         ) extends Schema


object GenericDerivation {
  implicit val jsonEncoder: Encoder[Schema] = Encoder.instance {
    case payload@Payload(_, _) => payload.asJson
    case registerResponse@RegisterResponse(_, _, _) => registerResponse.asJson
    case registerRequest@RegisterRequest(_) => registerRequest.asJson

    case currVal@CurrVal(_, _) => currVal.asJson
    case portfolioResponse@PortfolioResponse(_, _, _) => portfolioResponse.asJson
    case payloadPositions@PayloadPositions(_) => payloadPositions.asJson
    case positions@Positions(_, _, _, _, _, _, _, _, _, _, _) => positions.asJson

  }

  implicit val jsonDecoder: Decoder[Schema] =
    List[Decoder[Schema]](
      Decoder[Payload].widen,
      Decoder[RegisterResponse].widen,
      Decoder[RegisterRequest].widen,


      Decoder[CurrVal].widen,
      Decoder[PortfolioResponse].widen,
      Decoder[PayloadPositions].widen,
      Decoder[Positions].widen,
    ).reduceLeft(_ or _)
}
