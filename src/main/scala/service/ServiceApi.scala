package service

import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model.{AttributeKey, ContentTypes, HttpEntity, HttpRequest, HttpResponse, StatusCodes, Uri, headers}
import io.circe.syntax._
import akka.actor.ActorSystem
import akka.actor.Status.Success
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.Uri.Query
import akka.http.scaladsl.model.headers.{Authorization, OAuth2BearerToken}
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Balance
import io.circe.generic.auto._
import slick.jdbc.JdbcBackend
import storage.{DataBaseQuery, Users}
import schema._
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

import scala.util.{Failure, Success}
import scala.concurrent.{ExecutionContext, Future}

class ServiceApi(rootUrl: String, token: String)
                (implicit ex: ExecutionContext, ac: ActorSystem, materializer: ActorMaterializer.type) {

  val auth: Authorization = headers.Authorization(OAuth2BearerToken(token))

  val request: HttpRequest = HttpRequest(
    uri = rootUrl,
    headers = Seq(auth)
  )

  //for sandbox only
  def register: Future[Either[ErrorResponse, RegisterResponse]] = {
    Http().singleRequest(request
      .withMethod(POST)
      .withUri(request.uri + "/sandbox/register")
      .withEntity(HttpEntity(ContentTypes.`application/json`, RegisterRequest("Tinkoff").asJson.noSpaces))).flatMap(res =>
      res.status match {
        case StatusCodes.OK =>
          Unmarshal(res).to[RegisterResponse].map(Right(_).withLeft[ErrorResponse])
        case StatusCodes.BadRequest =>
          Unmarshal(res).to[ErrorResponse].map(Left(_).withRight[RegisterResponse])
    })
  }

  def setCurrenciesBalance(brokerAccountId: String, balanceCurr: MoneyAmount): Future[Either[ErrorResponse, EmptyResponse]] ={
    Http().singleRequest(request
      .withMethod(POST)
      .withUri(Uri(request.uri + "/sandbox/currencies/balance").withQuery(Query(Map("brokerAccountId" -> brokerAccountId))))
      .withEntity(HttpEntity(ContentTypes.`application/json`, balanceCurr.asJson.noSpaces))).flatMap(res =>
      res.status match {
        case StatusCodes.OK =>
          Unmarshal(res).to[EmptyResponse].map(Right(_).withLeft[ErrorResponse])
        case _ =>
          Unmarshal(res).to[ErrorResponse].map(Left(_).withRight[EmptyResponse])
      })
  }

  def setPositionBalance(balancePos: SandboxSetPositionBalanceRequest): Future[Either[ErrorResponse, EmptyResponse]] ={
    Http().singleRequest(request
      .withMethod(POST)
      .withUri(request.uri + "/sandbox/positions/balance")
      .withEntity(HttpEntity(ContentTypes.`application/json`, balancePos.asJson.noSpaces))).flatMap(res =>
      res.status match {
        case StatusCodes.OK =>
          Unmarshal(res).to[EmptyResponse].map(Right(_).withLeft[ErrorResponse])
        case _ =>
          Unmarshal(res).to[ErrorResponse].map(Left(_).withRight[EmptyResponse])
      })
  }
  /////


  def getPortfolio(brokerAccountId: String): Future[Either[ErrorResponse, PortfolioResponse]] = {
    Http().singleRequest(request
      .withMethod(GET)
      .withUri(Uri(request.uri + "/portfolio").withQuery(Query(Map("brokerAccountId" -> brokerAccountId)))))
      .flatMap(res =>
    res.status match {
      case StatusCodes.OK =>
        Unmarshal(res).to[PortfolioResponse].map(Right(_).withLeft[ErrorResponse])
      case _ =>
        Unmarshal(res).to[ErrorResponse].map(Left(_).withRight[PortfolioResponse])
    })
  }

  def searchMarketInstrumentByTicket(ticker : String): Future[Either[ErrorResponse, MarketInstrumentListResponse]]  ={
    Http().singleRequest(request
      .withMethod(GET)
      .withUri(Uri(request.uri + "/market/search/by-ticker").withQuery(Query(Map("ticker" -> ticker)))))
      .flatMap(res =>
      res.status match {
        case StatusCodes.OK =>
          Unmarshal(res).to[MarketInstrumentListResponse].map(Right(_).withLeft[ErrorResponse])
        case _ =>
          Unmarshal(res).to[ErrorResponse].map(Left(_).withRight[MarketInstrumentListResponse])
      })
  }

  def createMarketOrder(figi: String, lots : Int, operation: String): Future[Either[ErrorResponse, OrderResponse]] = {
    Http().singleRequest(request
      .withMethod(POST)
      .withUri(Uri(request.uri + "/orders/market-order").withQuery(Query(Map("figi" -> figi))))
      .withEntity(HttpEntity(ContentTypes.`application/json`, MarketOrderRequest(lots, operation).asJson.noSpaces)))
      .flatMap(res =>
        res.status match {
          case StatusCodes.OK =>
            Unmarshal(res).to[OrderResponse].map(Right(_).withLeft[ErrorResponse])
          case _ =>
            Unmarshal(res).to[ErrorResponse].map(Left(_).withRight[OrderResponse])
        })
  }

  def createLimitOrder(figi: String, lots : Int, operation: String, price: Double): Future[Either[ErrorResponse, OrderResponse]] = {
    Http().singleRequest(request
      .withMethod(POST)
      .withUri(Uri(request.uri + "/orders/market-order").withQuery(Query(Map("figi" -> figi))))
      .withEntity(HttpEntity(ContentTypes.`application/json`, LimitOrderRequest(lots, operation, price).asJson.noSpaces)))
      .flatMap(res =>
        res.status match {
          case StatusCodes.OK =>
            Unmarshal(res).to[OrderResponse].map(Right(_).withLeft[ErrorResponse])
          case _ =>
            Unmarshal(res).to[ErrorResponse].map(Left(_).withRight[OrderResponse])
        })
  }


  ///MSG
  def portfolioMsg(portfolioResp: Either[ErrorResponse, PortfolioResponse]): Future[String] = {
    portfolioResp match {
      case Left(e) => Future.successful(s"Error: code: ${e.payload.code}, message: ${e.payload.message}")
      case Right(value) => Future.successful(value.payload.positions.map{x =>
        s"Type: ${x.instrumentType}, Ticker: ${x.ticker}, FIGI: ${x.figi}, " +
          s"Lots: ${x.lots}, Balance: ${x.balance}"}.mkString("\n"))
    }
  }

//  def message[T](data: Future[Either[ErrorResponse, T]]): Future[String] ={
//    data.map{
//      case Left(e) => s"Error $e"
//      case Right(value) => parseResponse(value)
//    }
//  }
//
//  def parseResponse[T](resp: T): String = {
//    resp match {
//      case MarketInstrumentListResponse =>
//    }
//  }


}

