# StopLoss/TakeProfit
## ToDo
- Генератор http запросов
- Подключение к портфелю
- Получение тикеров бумаг в портфеле и объем валюты (баланс)
- Выбор тикера для SL/TP
- Ввод цены продажи 
- Создание новой заявки на продажу
- Уведомление о срабатывании заявки в телеграмме (бот)
- Обновление баланса портфеля
## Links
Tinkoff API DOC
https://tinkoffcreditsystems.github.io/invest-openapi/